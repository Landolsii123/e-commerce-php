<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Connexion</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="../images/logo.png" />
</head>

<body id='bodyconnexion'>
<?php include 'header.php'; ?>
	<article id='articleconnexion'>
	<div align="middle" class="connect">
	<form action="submit.php" method="post" id='formconnexion'>
		<label id="label" for="username">Pseudonyme : </label><input id="input" type="text" id="username" name="username"><br><br>
		<label id="label" for="password">Mot de passe : </label><input id="input" type="password" id="password" name="password"><br><br>
		<span id="check"><input type="checkbox"> Remember me</span><br><br>
		<input class="btn" type="submit" value="Se connecter"><br><br><br>

		<label id="sec"><a href="#">Mot de passe oublié ?</a></label><label id="sec">|</label><label id="sec"><a href="inscription.php">S'inscrire</a></label>
	
	</form>
	</div>
</article>
<?php include 'footer.php'; ?>
</body>

</html>