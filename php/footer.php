<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="icon" type="image/png" href="../images/logo.png" />
	
	<title>Origen</title>
</head>
<body>
<footer>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<ul>
						<li><a href="#" class="heading">Boutique près de chez vous</a></li>
						<li><a href="#" class="heading">Inscription aux emails</a></li>
						<li><a href="#" class="heading">Devenir Membre</a></li>
						<li><a href="#" class="heading">Carte Cadeaux</a></li>
					</ul>
				</div>

				<div class="col-md-3">
					<ul>
						<li><a href="#" class="heading">A L'aide</a></li>
						<li><a href="#">Expédition et livraisin</a></li>
						<li><a href="#">Retours</a></li>
						<li><a href="#">Modes de paiment</a></li>
						<li><a href="#">Informations sur le recyclage</a></li>
						<li><a href="#">Nous contacter</a></li>
					</ul>
				</div>

				<div class="col-md-3">
					<ul>
						<li><a href="#" class="heading">INFOS</a></li>
						<li><a href="#" class="heading">à Propos de nous</a></li>
						<li><a href="#">Retours</a></li>
						<li><a href="#">Carrières</a></li>
						<li><a href="#">Investisseurs</a></li>
					</ul>
				</div>

				<div class="col-md-3 social">
					<div class="heading">Réseaux sociaux</div>
					<div>
					<a href="#" ><img class="icon" src="../images/1449950096_facebook_online_social_media.ico" ></a>
					<a href="#" ><img class="icon" src="../images/1449950314_online_social_media_twitter.ico" ></a>
					<a href="#" ><img class="icon" src="../images/google_plus.ico" ></a>
					</div>
				</div>

			</div>
			<div class="split">
				<div class="row bottom">
					<div class="col-md-5">
						© 2015 Origen , Inc. Tous droits réservés
					</div>
					<div class="col-md-7">
						<ul>
							<li><a href="#">Guides</a></li>
							<li><a href="#">Conditions générales</a></li>
							<li><a href="#">Mentions légales</a></li>
							<li><a href="#">Politique de confidentialité et en matière de cookies</a></li>
							<li><a href="#">Paramètres de cookies</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>
</body>
</html>