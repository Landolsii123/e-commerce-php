<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content=" width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/produit.css">
	<link rel="icon" type="image/png" href="../images/logo.png" />


	<title>Origen</title>
</head>
<style type="text/css">
	.foo{
	margin-left: -20px;
	padding: 0px;
	width: 100em;
}
</style>
<?php include 'header.php'; ?>
	<div class="container-fluid" id="wrapper">
	 <div class="row">
	  <div class="col-md-6">
	  	<?php 
	  		$id=$_GET["id"];

				try
				{
					$bdd = new PDO('mysql:host=localhost;dbname=site;charset=utf8', 'root', '');
				}
				catch (Exception $e)
				{	
    				die('Erreur : ' . $e->getMessage());
				}
				$query=$bdd->prepare("SELECT * from produit as p  where p.id= ? ");
				$query->execute([$id]);
				$r = $query->fetch(PDO::FETCH_OBJ);
	  	 ?>
	 	<img class="imgs" src="<?php echo "$r->photo" ?> " alt="">
	  </div>
	 
		
		<div class="col-md-6 desc">
			<h1><?php echo "$r->nom"; ?></h1>
			<h5><i>CHAUSSURE POUR HOMME</i></h5>
			<span id="price"><?php echo "$r->prix"; ?><sup>TND</sup></span>
			 <span class="rating">
        		<input type="radio" class="rating-input" id="rating-input-1-5" name="rating-input-1"/>
        		<label for="rating-input-1-5" class="rating-star"></label>
        		<input type="radio" class="rating-input" id="rating-input-1-4" name="rating-input-1"/>
        		<label for="rating-input-1-4" class="rating-star"></label>
        		<input type="radio" class="rating-input" id="rating-input-1-3" name="rating-input-1"/>
        		<label for="rating-input-1-3" class="rating-star"></label>
        		<input type="radio" class="rating-input" id="rating-input-1-2" name="rating-input-1"/>
        		<label for="rating-input-1-2" class="rating-star"></label>
        		<input type="radio" class="rating-input" id="rating-input-1-1" name="rating-input-1"/>
        		<label for="rating-input-1-1" class="rating-star"></label>
			</span>
			<hr>
			  <span>Noir/Or métallique/Anthracite/Noir</span>
			  <span>Référence : 538416-007</span><br><br>
			<div class="btn-o">
			  <span class="label taille">
  					<select name="taille" class="">
  						<option>Taille</option>
  						<option value="">EU 38</option>
  						<option value="">EU 38.5</option>
  						<option value="">EU 39</option>
  						<option value="">EU 39.5</option>
  						<option value="">EU 40</option>
  						<option value="">EU 40.5</option>
  						<option value="">EU 41</option>
  						<option value="">EU 41.5</option>
  						<option value="">EU 42</option>
  						<option value="">EU 42.5</option>
  						<option value="">EU 43</option>
  						<option value="">EU 43.5</option>
  						<option value="">EU 45</option>
  					</select>
			   </span>

			   <span class="label qte">
			   		<select name="qte" class="">
			   			<option>Qté</option>
			   			<option value="">1</option>
			   			<option value="">2</option>
			   			<option value="">3</option>
			   			<option value="">4</option>
			   			<option value="">5</option>
			   			<option value="">6</option>
			   			<option value="">7</option>
			   			<option value="">8</option>
			   			<option value="">9</option>
			   			<option value="">10</option>
			   		</select>
			   </span>
			   <span id="button">
			   	<button class="panier">AJOUTER AU PANIER</button>
			   </span>
			  </div>
	 </div>
	</div>
	<br>
<hr>
<br>
	<div class="container-fluid">
	  <div class="row">
	  <div class="col-md-4 imEU ">
	  	<img src="<?php echo $r->photo ?>" alt="" width="250px" >
	  </div>
		
		<div class="col-md-8">
			<h2>DESIGN CLASSIQUE. AMORTI MAXIMUM.</h2>
			<p>
				La chaussure Nike Air Max 95 Premium pour Homme est dotée de revêtements en mesh et en cuir pour plus de confort et un look unique. Les unités Air-Sole et la semelle intermédiaire sur toute la longueur ajoutent un excellent amorti à cette chaussure légère.
			</p>
			<h4>AVANTAGES</h4>
			<ul>
				<li>Unités Air-Sole au talon et à l'avant-pied pour un amorti en toute légèreté</li>
				<li>Empeigne en mesh pour plus de ventilation</li>
				<li>Renforts en cuir synthétique pour plus de maintien et de résistance</li>
				<li>Semelle intermédiaire sur toute la longueur pour une résistance, une stabilité et un amorti accrus</li>
			</ul>

			<h4>CARACTÉRISTIQUES DU PRODUIT</h4>
			<ul>
				<li>Semelle extérieure à motif gaufré pour une meilleure adhérence et plus de résistance</li>
				<li>Rainures flexibles sur la semelle extérieure pour une meilleure articulation, une plus grande souplesse et une transition plus fluide</li>
				<li>Plaque en TPU pour un meilleur maintien et une plus grande stabilité</li>
			</ul>
		</div>
		</div>
	</div>
<div class="foo"><?php include 'footer.php'; ?></div> 
</html>