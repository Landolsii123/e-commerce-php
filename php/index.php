<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/style.css">

	<link rel="icon" type="image/png" href="../images/logo.png" />
	<script src="../js/jquery-2.1.3.js"></script>
	<script src="../js/bootstrap.min.js"></script>


	<title>Origen</title>
</head>
<?php include 'header.php'; ?>

  

	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" id="slider">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="../images/back1.jpg" height="100%" width="100%" alt="...">
      <div class="carousel-caption">
        
      </div>
    </div>
    <div class="item">
      <img src="../images/winter2.jpg" height="100%" width="100%" alt="...">
      <div class="carousel-caption">
        
      </div>
    </div>
	
	<div class="item">
      <img src="../images/01-woman.jpg" height="100%" width="100%" alt="...">
      <div class="carousel-caption">
        
      </div>
    </div>

    <div class="item">
      <img src="../images/winter3.jpg" height="100%" width="100%" alt="...">
      <div class="carousel-caption">
        
      </div>
    </div>

    <div class="item">
      <img src="../images/slide3.jpg" height="100%" width="100%" alt="...">
      <div class="carousel-caption">
        
      </div>
    </div>
    
  </div>

  <!-- Controls -->
</div>

	
	<script>
		$(document).ready(function(){
			$('.carousel').carousel({
		  		interval: 2500
			})
		});
	</script>
<?php include 'footer.php'; ?>
</html>
