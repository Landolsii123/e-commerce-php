<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="../images/logo.png" />
    <title>Origen</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../css/css/3-col-portfolio.css" rel="stylesheet">


</head>
 
<body>

    <?php include 'header.php'; ?>
    <?php
        $id=$_GET["id"];

try
{
    $bdd = new PDO('mysql:host=localhost;dbname=site;charset=utf8', 'root', '');
}
catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}
$query=$bdd->prepare("SELECT * from produit as p , produit_sous_sous_cathegorie as pss where p.id=pss.idp and pss.idssc = ? ");
$query->execute([$id]); 
     ?>
   
    <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">BASKETS LIFESTYLE POUR HOMME</h1>
            </div>
        </div>
        <!-- /.row -->
        <?php while ($r = $query->fetch(PDO::FETCH_OBJ)){  ?>
        <!-- Projects Row -->
        <div class="">
            <div class="col-md-4 portfolio-item">
                <a href="produit.php?id=<?php echo "$r->id";?> "><img class="img-responsive prod" src="<?php echo "$r->photo" ?>" ></a>
                <h3><a href="#"><?php echo "$r->nom ";?></a></h3>
                <h4><em>Chaussures pour Hommes</em></h4>
                <h2><?php echo "$r->prix"; ?><sup>TND</sup></h2>   
            </div>
            <?php } ?>
        </div>
        <!-- /.row -->
       
        

        <!-- Pagination -->

        <div class="row text-center">
            <div class="col-lg-12">
                <hr>
                <ul class="pagination">
                    <li>
                        <a href="#">&laquo;</a>
                    </li>
                    <li class="active">
                        <a href="#">1</a>
                    </li>
                    <li>
                        <a href="#">2</a>
                    </li>
                    <li>
                        <a href="#">3</a>
                    </li>
                    <li>
                        <a href="#">4</a>
                    </li>
                    <li>
                        <a href="#">5</a>
                    </li>
                    <li>
                        <a href="#">&raquo;</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.row -->



             
        
    <!-- jQuery -->
    <script src="../js/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/js/bootstrap.min.js"></script>

</body>

<div class="foo"> <?php include 'footer.php' ?></div>

</html>
 
