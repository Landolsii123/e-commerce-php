<?php include_once "init.php" ?>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="icon" type="image/png" href="../images/logo.png" />

	<title>ORIGEN ! Welcome</title>

</head>
<body>
	<?php 
	if (loggedin()) {
		include "loggedin.php";
	}
	
	?>
	<div>
		<ul id="nav" class="dropdown dropdown-linear dropdown-columnar">
			<li id="logo"><a href="index.php"><img class="origen" src="../images/logo.png" alt="" align="middle"></a></li>
			<li><pre  class="dir">HOMMES</pre>
				<ul>
					<li class="dir sep">NOUVEAUTÉS
					 <ul>
					 	<li><strong><a href="../construction.html">SORTIES À VENIR</a></strong></li>
					 	<li><strong><a href="../construction.html">MEILLEURES VENTES</a></strong></li>
					 	<li><strong><a href="../construction.html">À NOUVEAU DISPONIBLE</a></strong></li>
					 	<li><strong><a href="../construction.html">PROMOTIONS</a></strong></li>
					 </ul>
					</li>
					<li class="dir">CHAUSSURES
					 <ul>
					 	<li><a href="affiche_P.php?id=1">Lifestyle</a></li>
					 	<li><a href="affiche_P.php?id=2">Running</a></li>
					 	<li><a href="affiche_P.php?id=3">Football</a></li>
					 	<li><a href="affiche_P.php?id=4">Basket-ball</a></li>
					 	<li><a href="affiche_P.php?id=5">Fitness et training</a></li>
					 	<li><a href="affiche_P.php?id=6">Skateboard</a></li>
					 </ul>
					</li>
					<li class="dir">VÊTEMENTS
					 <ul>
						<li><a href="affiche_P.php?id=7">Maillots d'équipe</a></li>
						<li><a href="affiche_P.php?id=8">Sweats à capuche et sweat-shirts</a></li>
						<li><a href="affiche_P.php?id=9">Vestes et vestes sans manches</a></li>
						<li><a href="affiche_P.php?id=10">Hauts et tee-shirts</a></li>
						<li><a href="affiche_P.php?id=11">Pantalons et collants</a></li>
						<li><a href="affiche_P.php?id=12">Shorts</a></li>		
					 </ul>
					</li>
				</ul>
			</li>
			<li><pre class="dir">FEMMES</pre>
				<ul>
					<li class="dir sep">NOUVEAUTÉS
					 <ul>
					 	<li><strong><a href="../construction.html">SORTIES À VENIR</a></strong></li>
					 	<li><strong><a href="../construction.html">MEILLEURES VENTES</a></strong></li>
					 	<li><strong><a href="../construction.html">À NOUVEAU DISPONIBLE</a></strong></li>
					 	<li><strong><a href="../construction.html">PROMOTIONS</a></strong></li>
					 </ul>
					</li>
					<li class="dir">CHAUSSURES
					 <ul>
					 	<li><a href="affiche_P.php?id=13">Lifestyle</a></li>
					 	<li><a href="affiche_P.php?id=14">Running</a></li>
					 	<li><a href="affiche_P.php?id=15">Football</a></li>
					 	<li><a href="affiche_P.php?id=16">Basket-ball</a></li>
					 	<li><a href="affiche_P.php?id=17">Fitness et training</a></li>
					 	<li><a href="affiche_P.php?id=18">Skateboard</a></li>
					 </ul>
					</li>
					<li class="dir">VÊTEMENTS
					 <ul>
						<li><a href="affiche_P.php?id=19">Brassières</a></li>
						<li><a href="affiche_P.php?id=20">Hauts et tee-shirts</a></li>
						<li><a href="affiche_P.php?id=21">Vestes et vestes sans manches</a></li>
						<li><a href="affiche_P.php?id=22">Sweats à capuche et sweat-shirts</a></li>
						<li><a href="affiche_P.php?id=23">Pantalons et collants</a></li>
						<li><a href="affiche_P.php?id=24">Shorts</a></li>
						<li><a href="affiche_P.php?id=25">Jupes et robes</a></li>		
					 </ul>
					</li>
				</ul>
			</li>
			<li><pre class="dir">ENFANTS</pre>
				<ul>
					<li class="dir sep">NOUVEAUTÉS
					 <ul>
					 	<li><strong><a href="../construction.html">SORTIES À VENIR</a></strong></li>
					 	<li><strong><a href="../construction.html">MEILLEURES VENTES</a></strong></li>
					 	<li><strong><a href="../construction.html">À NOUVEAU DISPONIBLE</a></strong></li>
					 	<li><strong><a href="../construction.html">PROMOTIONS</a></strong></li>
					 </ul>
					</li>
					<li class="dir">ÂGE
					 <ul>
					 	<li><a href="../construction.html">Bébés et Petits enfants</a></li>
					 	<li><a href="../construction.html">Jeunes (3-8 ans)</a></li>
					 	<li><a href="../construction.html">plus âgés (8-15 ans)</a></li>
					 </ul>
					</li>
					<li class="dir">CHAUSSURES
					 <ul>
					 	<li><a href="afficheC.php?id=26">Lifestyle</a></li>
					 	<li><a href="afficheC.php?id=27">Running</a></li>
					 	<li><a href="afficheC.php?id=28">Football</a></li>
					 	<li><a href="afficheC.php?id=29">Basket-ball</a></li>
					 	<li><a href="afficheC.php?id=30">Fitness et training</a></li>
					 	<li><a href="afficheC.php?id=31">Skateboard</a></li>
					 </ul>
					</li>
					<li class="dir">VÊTEMENTS
					 <ul>
						<li><a href="afficheC.php?id=32">Maillots d'équipe</a></li>
						<li><a href="afficheC.php?id=33">Sweats à capuche et sweat-shirts</a></li>
						<li><a href="afficheC.php?id=34">Vestes et vestes sans manches</a></li>
						<li><a href="afficheC.php?id=35">Hauts et tee-shirts</a></li>
						<li><a href="afficheC.php?id=36">Pantalons et collants</a></li>
						<li><a href="afficheC.php?id=37">Shorts</a></li>		
					 </ul>
					</li>
				</ul>
				<li class="dir" id="contact"><a href="contact.php"><pre>CONTACTEZ NOUS</pre></a></li>
				<li id="search">
				 <form id="searchbox" method="POST" action="recherche.php" autocomplete="off" >
					<input name="q" type="text" size="15" placeholder="recherche..." />
					<!-- <input id="button-submit" type="submit" value=" "/> -->
				</form>
				</li>
			</li>
			<?php 
			 if (!loggedin()) 
			 {
		
	
			echo' <div>';
			     echo '<a class="inscr" href="connexion.php">S`inscrire/Se connecter</a>';
			echo '</div>';
		}
			?>
		</ul>
	   
	</div>


</body>

</html>