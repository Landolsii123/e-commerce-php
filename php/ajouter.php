<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/ajouter.css">
	<link rel="icon" type="image/png" href="../images/logo.png" />
	<script src="../js/jquery-2.1.3.js"></script>
	<script src="../js/bootstrap.min.js"></script>


	<title>Origen</title>
</head>
<?php include 'header.php'; ?>

<article>
<form action="ajout.php" method="POST" >
<span id="cath_">Cathégorie :    </span>
<select name="cath" id="">
	<option value="lifestyleh">Homme Lifestyle</option>
	<option value="runningh">Homme Running</option>
	<option value="footballh">Homme Football</option>
	<option value="basketh">Homme Basket-ball</option>
	<option value="fitnessh">Homme Fitness</option>
	<option value="skateboardh">Homme Skateboard</option>
	<option value="maillotsh">Homme Maillots d'équipe</option>
	<option value="Sweatch">Homme Sweats</option>
	<option value="vestesh">Homme Vestes</option>
	<option value="hautsh">Homme T-shirt</option>
	<option value="pantalonsh">Homme Pantalons</option>
	<option value="shortesh">Homme Shorts</option>
	<option value="lifesylef">Femme Lifestyle</option>
	<option value="runningf">Femme Running</option>
	<option value="footballf">Femme Football</option>
	<option value="basketf">Femme Basket-ball</option>
	<option value="fitnessf">Femme Fitness</option>
	<option value="skateboardf">Femme Skateboard</option>
	<option value="Brassièresf">Femme Brassière</option>
	<option value="Hautsf">Femme T-Shirt</option>
	<option value="Vestsf">Femme Vestes</option>
	<option value="Sweatsf">Femme Sweats</option>
	<option value="Pantalonsf">Femme Pantalons</option>
	<option value="Shortsf">Femme Shorts</option>
	<option value="Jupesf">Femme Robes</option>
	<option value="lifestylee">Enfant Lifestyle</option>
	<option value="runninge">Enfant Running</option>
	<option value="footballe">Enfant Football</option>
	<option value="baskete">Enfant Baskt-Ball</option>
	<option value="fitnesse">Enfant Fitness</option>
	<option value="skateboarde">Enfant Skateboard</option>
	<option value="maillotse">Enfant Maillots d'équipe</option>
	<option value="Sweatse">Enfant Sweats</option>
	<option value="vestese">Enfant Vestes</option>
	<option value="hautse">Enfant T-shirt</option>
	<option value="pantalonse">Enfant Pantalons</option>
	<option value="shortse">Enfant Shorts</option>
</select>	<br>
<br>

<label>Nom du produit : </label> <input type="text" value= "" name="nom"><br><br>	
<label>Prix du produit : </label><input type="text" value= "" name="prix">	<br><br>
<label>Taille du produit : </label><input type="text" value= "" name="taille"><br><br>
<label>Description : </label><textarea name="description" id="" cols="30" rows="10"></textarea><br><br>	
<label>Disponible : </label><input type="text" value= "" name="dispo"><br><br>
<label>Année : </label><input type="text" value= "" name="annee"><br><br>
<label>Les liens des photos :  </label><input type="text" value="" name="photo"><br><br>

<input class="btn" type="submit" value="Enregistrer"><br>	

</article>

<?php include 'footer.php'; ?>
</html>
