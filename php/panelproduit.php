<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap2.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/produit.css">
	<link rel="icon" type="image/png" href="images/logo.png" />
	<script src="js/jquery-2.1.3.js"></script>
	<script src="js/bootstrap.min2.js"></script>
	<title>ORIGEN ! Welcome</title>
</head>
<style type="text/css">
	#table{
		margin-top: 70px;
		margin-bottom: 500px;
	}
	#bod{
		background-image: url(../images/hero-2.jpg);
	}
	th{
		font-weight: bold;
		font-size: 20px;
		background-color: #5A5A5A;
		color: white;
		padding:120px;
	}
	.tr {
		padding:120px;
	}
	#d{
		font-weight: bold;
	}

	.control{
	height: 30px;
	width:30px;
	border-radius: 15px;
    }

</style>
<?php
include 'header.php';
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=site;charset=utf8', 'root', '');
}
catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}
$query=$bdd->prepare("SELECT * from produit ");
$query->execute();
?>
<body id="bod">
<table id="table">
<tr class="tr">
	<th id="d">ID</th>
	<th>Nom</th>
	<th>Description</th>
	<th>prix</th>
	<th>Pic</th>
	<th>année</th>
	<th>control</th>
</tr>
<?php
while ($r = $query->fetch(PDO::FETCH_OBJ)){

	echo "<tr class='panelAffichage'>";
	
		echo "<td>".$r->id."</td>";
		echo "<td padding='5px'>".$r->nom."</td>";
		echo "<td>".$r->descr."</td>";
		echo "<td>".$r->prix."</td>";
		echo "<td><img id='imageP' src=".$r->photo."></td>";
		echo "<td>".$r->annee."</td>";
		echo "<td><a href='modifier.php?id=".$r->id."'><img src='../images/edit.jpg' class='control' ></a>  <a href='supp.php?id=".$r->id."'><img src='../images/supprime.jpg' class='control'></td>";
	echo "</tr>";

	
}
echo "</table>";
echo "</body>";
include 'footer.php';

?>
</html>